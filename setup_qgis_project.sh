#!/bin/bash
#Including .ini file
. config.ini


echo "+---------------------------+"
echo "| Génération du Projet QGIS |"
echo "|                           |" 
echo "|   /\                 /\   |"
echo "|  / \'._   (\_/)   _.'/ \  |"
echo "|  |.''._'--(o.o)--'_.''.|  |"
echo "|  \_ /   ;=/    \=; \ _/   |"
echo "|     \__|  \___/ |__/      |"
echo "|          \(_|_)/          |"
echo "|           \" \` \"           |"
echo "+---------------------------+"
echo ""

if [[ -f $QGISFILENAME ]]; then
    echo "un fichier existe déjà"
    read -p 'Voulez-vous le supprimer (O/N): ' deletefile
    if [[ $deletefile != O ]]; then
        echo "Pas de changement"
        exit
    elif [[ $deletefile == O ]]; then
        rm $QGISFILENAME
    fi
fi

cp projet_dbchiro.qgs.template $QGISFILENAME
sed -i "s/MYDBNAME/$MYDBNAME/g" $QGISFILENAME
sed -i "s/MYDBHOST/$MYDBHOST/g" $QGISFILENAME
sed -i "s/MYDBPORT/$MYDBPORT/g" $QGISFILENAME
sed -i "s/MYDBUSER/$MYDBUSER/g" $QGISFILENAME
sed -i "s/MYDBPASSWORD/$MYDBPASSWORD/g" $QGISFILENAME
echo "Vous pouvez maintenant utiliser le fichier $QGISFILENAME"
