-- données détaillées de captures
set search_path to 'views';

drop MATERIALIZED VIEW views.donnees_capture;
CREATE MATERIALIZED VIEW views.donnees_capture
  AS (
    SELECT distinct
      scd.id_countdetail,
      ses.date_start                           date,
      EXTRACT(YEAR FROM ses.date_start)        an,
      EXTRACT(MONTH FROM ses.date_start)       mois,
      s2.id_place,
      s2.name                                  localite,
     /* st_x(st_transform(s2.geom, 2154)) :: INT x_l93,
      st_y(st_transform(s2.geom, 2154)) :: INT y_l93,*/
      s2.altitude,
      m2.name                                  commune,
      m2.code                                  insee,
      territory.code                           dpt,
      ds.codesp,
      ds.common_name_fr                        nom_fr,
      ds.sci_name                              nom_scien,
      ss.period,
      scd.time heure,
      c2.code                                  contact,
      scd.ab,
      ds2.code sexe,
      da.code age,
      ss.breed_colo                            colo_repro,
      ses.comment                              commentaire_session,
      ss.comment                               commentaire_obs,
      --string_agg(distinct(a.last_name || ' ' || a.first_name),', ')       observateurs,
      mm.name                                  etude,
      stat.dhff,
      stat.fr_regra_redlist                    lrra,
      s2.geom
    FROM PUBLIC.sights_sighting ss
      LEFT JOIN PUBLIC.dicts_specie ds ON ss.codesp_id = ds.id
      LEFT JOIN PUBLIC.sights_session ses ON ss.session_id = ses.id_session
      left join public.accounts_profile a on ses.main_observer_id=a.id
      LEFT JOIN PUBLIC.sights_session_other_observer ssoo ON ssoo.session_id = ses.id_session
      left join public.accounts_profile a2 on ssoo.profile_id = a2.id
      LEFT JOIN PUBLIC.sights_place s2 ON ses.place_id = s2.id_place
      LEFT JOIN PUBLIC.dicts_typeplace typeplace ON s2.type_id = typeplace.id
      LEFT JOIN PUBLIC.dicts_contact c2 ON ses.contact_id = c2.id
      LEFT JOIN PUBLIC.geodata_territory territory ON s2.territory_id = territory.id
      LEFT JOIN PUBLIC.dicts_speciestatus stat ON stat.specie_id = ss.codesp_id
      LEFT JOIN PUBLIC.geodata_municipality m2 ON s2.municipality_id = m2.id
      LEFT JOIN PUBLIC.management_study mm ON mm.id_study = ses.study_id
    left join public.sights_countdetail scd on scd.sighting_id=ss.id_sighting
    left join public.dicts_sex ds2 on ds2.id=scd.sex_id
    left join public.dicts_age da on da.id=scd.age_id
where c2.code in ('ca','vm') and territory.coverage is true

  );

GRANT USAGE ON SCHEMA public, views TO consultall; GRANT SELECT ON ALL TABLES IN SCHEMA public, views TO consultall;
