-- vérifier les localités réseaux de gîtes + définition
set search_path to 'views';

-- creation des tables dico
drop table if exists cs_hierar_temp_se;

create temporary table cs_hierar_temp_se (
  codesp  text,
  note_se integer
)
;

insert into cs_hierar_temp_se values
  ('minsch', 3), ('myoalc', 3), ('myobec', 3), ('myocap', 3), ('myomyo', 2), ('nyclas', 2), ('tadten', 2),
  ('myobra', 2), ('myonat', 2), ('plemac', 2), ('pleaur', 2), ('myobly', 2), ('rhieur', 2), ('barbar', 1),
  ('rhifer', 1), ('myomys', 1), ('myoema', 1), ('myodau', 1), ('nycnoc', 1), ('nyclei', 1), ('pleaus', 1),
  ('rhihip', 1), ('pipnat', 1), ('pippyg', 1), ('vesmur', 1), ('eptser', 1), ('eptnil', 1), ('hypsav', 1),
  ('pippip', 0), ('pipkuh', 0), ('pipind', 0), ('pleind', 1), ('murmur', 2)
;

drop table if exists cs_hierar_temp_resp_reg
;

create temporary table cs_hierar_temp_resp_reg (
  region  text,
  codesp  text,
  note_rr integer
)
;

insert into cs_hierar_temp_resp_reg values
  /*par rapport à la méthodo nationale, ajout taxon murmur, en prenant la note myomyo pour centre et moyenne pour se*/
  ('centre', 'barbar', 2), ('centre', 'myomyo', 3), ('centre', 'rhifer', 2), ('centre', 'nyclas', 2),
  ('centre', 'rhihip', 3), ('centre', 'minsch', 2), ('centre', 'tadten', 1), ('centre', 'myoema', 3),
  ('centre', 'myoalc', 1), ('centre', 'myobec', 2), ('centre', 'myonat', 1), ('centre', 'pleaur', 1),
  ('centre', 'myobly', 1), ('centre', 'rhieur', 2), ('centre', 'myobra', 1), ('centre', 'myonat', 1),
  ('centre', 'pleaur', 1), ('centre', 'rhihip', 3), ('centre', 'myomys', 2), ('centre', 'myodau', 1),
  ('centre', 'nycnoc', 2), ('centre', 'nyclei', 1), ('centre', 'pleaus', 1), ('centre', 'pipnat', 1),
  ('centre', 'pippyg', 1), ('centre', 'vesmur', 1), ('centre', 'eptser', 1), ('centre', 'hypsav', 2),
  ('centre', 'pippip', 1), ('centre', 'pipkuh', 1), ('centre', 'eptnil', 2), ('centre', 'murmur', 3),
  ('se', 'rhieur', 3), ('se', 'minsch', 4), ('se', 'myobly', 3), ('se', 'myomyo', 2), ('se', 'rhifer', 2),
  ('se', 'myoalc', 2), ('se', 'myobec', 2), ('se', 'myocap', 4), ('se', 'barbar', 2), ('se', 'nyclas', 2),
  ('se', 'tadten', 4), ('se', 'myoema', 3), ('se', 'myobra', 2), ('se', 'myonat', 2), ('se', 'plemac', 4),
  ('se', 'pleaur', 1), ('se', 'rhihip', 3), ('se', 'myomys', 1), ('se', 'myodau', 1), ('se', 'nycnoc', 1),
  ('se', 'nyclei', 2), ('se', 'pleaus', 2), ('se', 'pipnat', 2), ('se', 'pippyg', 3), ('se', 'vesmur', 2),
  ('se', 'eptser', 1), ('se', 'hypsav', 3), ('se', 'pippip', 1), ('se', 'pipkuh', 3), ('se', 'eptnil', 2),
  ('se', 'murmur', 2.5)
;

-- génération de la matrice de données
drop table if exists cs_hierarch_note_couple ;

create temporary table cs_hierarch_note_couple as (
  with
    --simplification de la géométrie zones biogéo
      zone_biogeo as (
        select
            case
            when nom_domain ilike '%med%'
              then 'se'
            else 'centre'
            end                                       region
          , st_transform(st_simplify(geom, 50), 4326) geom
        from admingcra.fr_zone_biogeo
    ),
    --selection des données exploitées hors réseau de gîtes
    /*possibilité de distinguer les gîtes en réseau en le spécifiant ici dans la requête et en en rajoutant une dataexploitréseau*/
      dataexploithorsreseau as (
        select distinct
          ss.id_sighting
          , p.id_place
          , case
            when ds.codesp ilike 'murmur'
              then 'myomyo'
            else ds.codesp
            end           codesp
          , ss.total_count
          , ss.breed_colo
          , case when ss.period ilike 'estivage'
          then 'e'
            when ss.period ilike 'hivernage'
              then 'h'
            when ss.period ilike 't%printa%'
              then 'tp'
            when ss.period ilike 't%au%'
              then 'ta'
            else null end periode_code
        /*loc.reseau_sit*/
        from sights_sighting ss
          left join sights_session ses
            on ss.session_id = ses.id_session
          left join sights_place p
            on ses.place_id = p.id_place
          left join dicts_contact dc
            on ses.contact_id = dc.id
          left join dicts_specie ds
            on ss.codesp_id = ds.id
          left join geodata_territory t
            on p.territory_id = t.id
          left join dicts_period dp
            on ss.period = dp.descr
        where
          p.is_gite = 'true'
          and dc.code not in ('vm', 'du')
          /*restriction aux 11 dernières années*/
          and abs(extract(year from current_date) - extract(year from ses.date_start)) <= 20
          and (ds.sp_true is true or ds.codesp ilike 'murmur')
      --  AND t.coverage IS TRUE
    ),
    --caractéristiques gîtes
      gite as (
        select distinct
          case when dp.category = 'bridge'
            then 'building'
          else dp.category end category
          , sp.geom
          , sp.id_place
          , sp.name
        from
          sights_place sp
          left join dicts_typeplace dp
            on sp.type_id = dp.id
        where
          sp.is_gite is true
    ),
    --agrégation des données
      dataexploit as (
        select *
        from dataexploithorsreseau
      /*possibilité d'inclure ici les données hors réseau via un UNION ALL*/
    ),
    --gite de transit
      gite_transit as (
        select
          d.id_place
          , d.codesp
          , string_agg(distinct (d.periode_code), ',') period
        from dataexploit d
        where d.periode_code ilike 't%'
        group by id_place, codesp
    ),
    -- préparation des données avec la colonne statut pour les données de transit
      datara_statut as (
      select distinct
        d.id_sighting
        , d.id_place
        , d.codesp
        , d.total_count
        , d.breed_colo
        , case
          when gt.period ilike 't%t%'
            then 'tts'
          when gt.period ilike 't%'
            then 'tt'
          else d.periode_code
          end statut_ok
      from
        dataexploit d
        left join gite_transit gt
          on (d.id_place = gt.id_place and d.codesp = gt.codesp)
      where
        d.periode_code ilike 't%'
      union all
      select distinct
        d.id_sighting
        , d.id_place
        , d.codesp
        , d.total_count
        , d.breed_colo
        , case
          when d.breed_colo is true
            then 'r'
          else d.periode_code
          end statut_ok
      from
        dataexploit d
      where
        d.periode_code not ilike 't%'
    ),
    -- preparation de la matrice d'évaluation temporaire
      eval_tempo as (
        select distinct
          d.id_place
          , string_agg(distinct (l.id_place :: text), ',') localite
          , d.codesp
          , max(d.total_count)                             n
          , D.statut_ok                                    type_gite
          , max(nr.note_rr)                                resp_regionale
          , max(se.note_se)                                sensibilite_se
          , max(nr.note_rr) + max(se.note_se)              ke
          , case
            when D.statut_ok in ('r', 'h', 'tts')
              then 2
            else 1
            end                                            tg
          , case
            when max(d.total_count) < 5
              then 0
            when max(d.total_count) < 20
              then 1
            when max(d.total_count) < 100
              then 2
            when max(d.total_count) < 300
              then 3
            when max(d.total_count) < 1000
              then 4
            when max(d.total_count) < 10000
              then 5
            when max(d.total_count) >= 10000
              then 6
            else null end                                  ic
        from datara_statut d
          inner join gite l
            on D.id_place = l.id_place
          left join zone_biogeo zb
            on st_within(l.geom, zb.geom)
          left join cs_hierar_temp_resp_reg nr
            on (nr.region = zb.region and nr.codesp ilike D.codesp)
          left join cs_hierar_temp_se se
            on d.codesp ilike se.codesp
        group by d.id_place, d.codesp, D.statut_ok
    )
  select distinct
    et.id_place
    , g.name
    , et.codesp
    , et.ke * (et.tg * et.ic) eval_sp
    , et.type_gite
  from eval_tempo et
    left join gite g
      on et.id_place = g.id_place
  order by et.id_place
)
;

-- suppression des données d'estivage pour les gîtes avec repro et estivage
with
    gite_estival as (
      select distinct
        id_place
        , codesp
        , string_agg(distinct (type_gite), ',') occupation
      from cs_hierarch_note_couple
      where type_gite in ('e', 'r')
      group by id_place, codesp
  ),
    gite_repro as (
      select distinct *
      from gite_estival
      where occupation ilike '%,%')
delete from cs_hierarch_note_couple n
where n.type_gite = 'e' and n.id_place || n.codesp in (
  select g.id_place || g.codesp
  from gite_repro g)
;

--evaluation finale
drop table if exists admingcra.cs_hierarchisation_finale
;

create table admingcra.cs_hierarchisation_finale as (
  with
      gite as (
      /*possibilité d'inclure les gîtes en réseau avec un UNION ALL*/
        select distinct
            t2.category type_gite
          , p.geom
          , p.id_place
          , p.id_place  nom_id
          , p.name
        from sights_place p
          left join sights_session ses
            on ses.place_id = p.id_place
          left join sights_sighting ss
            on ses.id_session = ss.session_id
          left join dicts_contact dc
            on ses.contact_id = dc.id
          left join dicts_specie ds
            on ss.codesp_id = ds.id
          left join geodata_territory t
            on p.territory_id = t.id
          left join dicts_typeplace t2
            on p.type_id = t2.id
        where /*
        spécifier condition sur le réeau de gîtes*/
          p.is_gite is true
          /*restriction aux 11 dernières années*/
          and abs(extract(year from current_date) - extract(year from ses.date_start)) <= 20
          and (ds.sp_true is true or ds.codesp ilike 'murmur')
      --      AND t.coverage IS TRUE
    ),
      eval_fin as (
        select distinct
          nsg.id_place
          , string_agg(distinct (loc.name), ',') localite
          , sum(eval_sp)                         note_tot
          , case
            when string_agg(distinct (loc.type_gite), ',') ilike 'c%,b%'
              then 'Mixte'
            when string_agg(distinct (loc.type_gite), ',') ilike 'b%,c%'
              then 'Mixte'
            when string_agg(distinct (loc.type_gite), ',') ilike 'c%'
              then 'Hypogé'
            else 'Epigé'
            end                                  type_gite
        from cs_hierarch_note_couple nsg
          left join gite loc
            on nsg.id_place = loc.id_place
        group by
          nsg.id_place
        order by note_tot desc
    )
  select distinct
    g.geom
    , e.*
    , case
      when e.type_gite in ('Hypogé', 'Mixte') and e.note_tot >= 200
        then '1 - Internationale'
      when e.type_gite in ('Hypogé', 'Mixte') and e.note_tot between 100 and 199
        then '2 - Nationale'
      when e.type_gite in ('Hypogé', 'Mixte') and e.note_tot between 50 and 99
        then '3 - Régionale'
      when e.type_gite = 'Epigé' and e.note_tot >= 100
        then '1 - Internationale'
      when e.type_gite = 'Epigé' and e.note_tot between 50 and 99
        then '2 - Nationale'
      when e.type_gite = 'Epigé' and e.note_tot between 30 and 49
        then '3 - Régionale'
      else '4 - Locale'
      end importance
  from eval_fin e left join gite g
      on e.id_place = g.nom_id
  --exclusion des gîtes disparus
  where note_tot is not null and e.id_place not in
                                 ('829', '2158', '2735', '6369', '6392', '7282', '7366', '8405', '9064', '9868', '9869', '18618', '26838')
  order by note_tot desc)
;

delete from views.cs_hierarchisation_finale;

insert into views.cs_hierarchisation_finale (select * from admingcra.cs_hierarchisation_finale);
